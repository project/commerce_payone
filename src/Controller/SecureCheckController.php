<?php

namespace Drupal\commerce_payone\Controller;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payone\Plugin\Commerce\PaymentGateway\PayoneCreditCardInterface;
use Drupal\Core\Access\AccessException;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides 3-D Secure check endpoints for credit card payments.
 */
class SecureCheckController extends ControllerBase {

  /**
   * Provides the "return" checkout payment page for 3-D Secure check.
   *
   * Redirects to the next checkout page, completing checkout.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The order.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function returnCheckoutPage(OrderInterface $commerce_order, Request $request) {
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $commerce_order->payment_gateway->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    if (!$payment_gateway_plugin instanceof PayoneCreditCardInterface) {
      throw new AccessException('The payment gateway for the order does not implement ' . PayoneCreditCardInterface::class);
    }

    /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
    $checkout_flow = $commerce_order->checkout_flow->entity;
    $checkout_flow_plugin = $checkout_flow->getPlugin();
    $checkout_flow_plugin_configuration = $checkout_flow_plugin->getConfiguration();
    $capture = $checkout_flow_plugin_configuration['panes']['payment_process']['capture'];

    try {
      $payment_gateway_plugin->onSecurityCheckReturn($commerce_order, $request, $capture);
      $redirect_step = $checkout_flow_plugin->getNextStepId('payment');
    }
    catch (PaymentGatewayException $e) {
      $this->getLogger('commerce_payone')->error($e->getMessage());
      $this->messenger()->addError($this->t('Payment failed at the payment server. Please review your information and try again.'));
      $redirect_step = $checkout_flow_plugin->getPreviousStepId('payment');
    }

    $checkout_flow_plugin->redirectToStep($redirect_step);
  }

  /**
   * Provides the "cancel" checkout payment page for 3-D Secure check.
   *
   * Redirects to the previous checkout page.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The order.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  public function cancelCheckoutPage(OrderInterface $commerce_order, Request $request) {
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $commerce_order->payment_gateway->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    if (!$payment_gateway_plugin instanceof PayoneCreditCardInterface) {
      throw new AccessException('The payment gateway for the order does not implement ' . PayoneCreditCardInterface::class);
    }

    $payment_gateway_plugin->onSecurityCheckCancel($commerce_order);
    /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
    $checkout_flow = $commerce_order->checkout_flow->entity;
    $checkout_flow_plugin = $checkout_flow->getPlugin();
    $previous_step_id = $checkout_flow_plugin->getPreviousStepId('payment');
    foreach ($checkout_flow_plugin->getPanes() as $pane) {
      if ($pane->getId() == 'payment_information') {
        $previous_step_id = $pane->getStepId();
      }
    }
    $checkout_flow_plugin->redirectToStep($previous_step_id);
  }

}
