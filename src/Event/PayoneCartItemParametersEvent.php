<?php

namespace Drupal\commerce_payone\Event;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the event used to alter cart item parameters.
 *
 * @see \Drupal\commerce_payone\Event\CommercePayoneEvents
 */
class PayoneCartItemParametersEvent extends Event {

  /**
   * The payment which is executed.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * The cart item parameters.
   *
   * @var string[]
   */
  protected $parameters;

  /**
   * Whether the payment is a partial one.
   *
   * @var bool
   */
  protected $partialPayment;

  /**
   * Constructs a new PayoneCartItemParametersRequest object.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment which is executed.
   * @param array $parameters
   *   The cart item parameters.
   * @param bool $partial_payment
   *   Whether this is a partial payment. Defaults to FALSE.
   */
  public function __construct(PaymentInterface $payment, array $parameters, $partial_payment = FALSE) {
    $this->payment = $payment;
    $this->parameters = $parameters;
    $this->partialPayment = $partial_payment;
  }

  /**
   * Gets the payment entity.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The payment for this event.
   */
  public function getPayment() {
    return $this->payment;
  }

  /**
   * Get the cart item parameters.
   *
   * @return string[]
   *   The cart item parameters.
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Whether the payment is a partial one.
   *
   * @return bool
   *   TRUE, if the payment is a partial one, FALSE otherwise.
   */
  public function isPartialPayment() {
    return $this->partialPayment;
  }

  /**
   * Set the cart item parameters.
   *
   * @param string[] $parameters
   *   The cart item parameters.
   */
  public function setParameters(array $parameters) {
    $this->parameters = $parameters;
  }

}
