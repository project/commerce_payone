<?php

namespace Drupal\commerce_payone\Event;

/**
 * Defines events for the Commerce Payone module.
 */
final class CommercePayoneEvents {

  /**
   * Name of the event fired when capture request to Payone is built.
   *
   * @Event
   */
  const PAYONE_CAPTURE_REQUEST = 'commerce_payone.capture_request';

  /**
   * Name of the event fired when preauthorization request to Payone is built.
   *
   * @Event
   */
  const PAYONE_PREAUTHORIZATION_REQUEST = 'commerce_payone.preauthorization_request';

  /**
   * Name of the event fired when debit request to Payone is built.
   *
   * @Event
   */
  const PAYONE_DEBIT_REQUEST = 'commerce_payone.debit_request';

  /**
   * Name of the event fired when refund request to Payone is built.
   *
   * @Event
   */
  const PAYONE_REFUND_REQUEST = 'commerce_payone.refund_request';

  /**
   * Name of the event fired when authorization request to Payone is built.
   *
   * @Event
   */
  const PAYONE_AUTHORIZATION_REQUEST = 'commerce_payone.authorization_request';

  /**
   * Name of the event fired for modifying the cart item parameters.
   *
   * @Event
   */
  const PAYONE_ALTER_CART_ITEM_PARAMETERS = 'commerce_payone.alter_cart_item_parameters';

  /**
   * Name of the event fired after a refund has been done.
   *
   * @Event
   */
  const PAYONE_REFUND_FINISHED = 'commerce_payone.refund.finished';

}
