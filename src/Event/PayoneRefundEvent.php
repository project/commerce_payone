<?php

namespace Drupal\commerce_payone\Event;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Price;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the event fired after a successful refund has been done.
 *
 * @see \Drupal\commerce_payone\Event\CommercePayoneEvents
 */
class PayoneRefundEvent extends Event {

  /**
   * The payment which has been refunded.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * The refund amount.
   *
   * @var \Drupal\commerce_price\Price
   */
  protected $amount;

  /**
   * Constructs a new PayoneRefundEvent object.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment which is refunded.
   * @param \Drupal\commerce_price\Price $amount
   *   The refund amount.
   */
  public function __construct(PaymentInterface $payment, Price $amount) {
    $this->payment = $payment;
    $this->amount = $amount;
  }

  /**
   * Gets the payment entity.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The payment for this event.
   */
  public function getPayment() {
    return $this->payment;
  }

  /**
   * Gets the refund amount.
   *
   * @return \Drupal\commerce_price\Price
   *   The refund amount.
   */
  public function getAmount() {
    return $this->amount;
  }

}
