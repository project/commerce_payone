<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the payment method type for Payone SEPA payments.
 *
 * @CommercePaymentMethodType(
 *   id = "commerce_payone_sepa",
 *   label = @Translation("Payone sepa payment"),
 *   create_label = @Translation("SEPA"),
 * )
 */
class SepaMethod extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    if ($payment_method->getRemoteId()) {
      return $this->t('SEPA mandate: @description', [
        '@description' => $payment_method->iban->value,
      ]);
    }
    else {
      return $this->t('New SEPA mandate');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['iban'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('IBAN'))
      ->setRequired(TRUE);

    $fields['bic'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('BIC'));

    return $fields;
  }

}
