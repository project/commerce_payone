<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the payment method type for Payone Sofort banking payments.
 *
 * @CommercePaymentMethodType(
 *   id = "commerce_payone_sofort",
 *   label = @Translation("Payone Sofort banking"),
 *   create_label = @Translation("Sofort banking"),
 * )
 */
class SofortMethod extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->t('Sofort banking');
  }

}
