<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the payment method type for Payone invoice payments.
 *
 * @CommercePaymentMethodType(
 *   id = "commerce_payone_invoice",
 *   label = @Translation("Payone Invoice"),
 *   create_label = @Translation("Invoice"),
 * )
 */
class InvoiceMethod extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    if (!$payment_method->getBillingProfile()->address->first()) {
      return $this->t('Invoice');
    }
    /** @var \Drupal\address\AddressInterface $address */
    $address = $payment_method->getBillingProfile()->address->first();
    $company = trim($address->getOrganization());
    $has_company = !empty($company);

    $invoice_label_parts = [];
    if ($has_company) {
      $invoice_label_parts[] = $company;
    }
    $invoice_label_parts[] = $address->getGivenName() . ' ' . $address->getFamilyName();
    $street = $address->getAddressLine1();
    if (!empty($address->getAddressLine2())) {
      $street .= ' ' . $address->getAddressLine2();
    }
    $invoice_label_parts[] = $street;
    $invoice_label_parts[] = $address->getPostalCode() . ' ' . $address->getLocality();
    $invoice_label_parts[] = $payment_method->dob->value;

    return $this->t('Invoice to @address', ['@address' => implode(', ', $invoice_label_parts)]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['dob'] = BundleFieldDefinition::create('datetime')
      ->setLabel($this->t('Date of birth'))
      ->setSetting('datetime_type', 'date')
      ->setRequired(TRUE);

    return $fields;
  }

}
