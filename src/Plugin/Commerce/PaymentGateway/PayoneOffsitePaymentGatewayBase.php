<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

use Drupal\address\AddressInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payone\ErrorHelper;
use Drupal\commerce_payone\Event\CommercePayoneEvents;
use Drupal\commerce_payone\Event\PayoneRequestEvent;
use Drupal\commerce_payone\PayoneApiServiceInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Abstract base class for any Payone offsite payment gateway.
 */
abstract class PayoneOffsitePaymentGatewayBase extends OffsitePaymentGatewayBase implements PayoneOffsitePaymentGatewayInterface, SupportsAuthorizationsInterface, SupportsRefundsInterface {

  use LoggerChannelTrait;
  use PayonePaymentGatewayConfigurationTrait;
  use PayonePaymentGatewayTrait;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The payment storage.
   *
   * @var \Drupal\commerce_payment\PaymentStorageInterface
   */
  protected $paymentStorage;

  /**
   * Constructs a new PayoneOffsitePaymentGatewayBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\commerce_payone\PayoneApiServiceInterface $api_service
   *   The Payone API service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    PayoneApiServiceInterface $api_service,
    EventDispatcherInterface $event_dispatcher
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->api = $api_service;
    $this->eventDispatcher = $event_dispatcher;
    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_payone.payment_api'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return $this->defaultPayonePaymentGatewayConfiguration()
      + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    return $this->buildPayonePaymentGatewayConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->submitPayonePaymentGatewayConfigurationForm($form, $form_state, $this->configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function initiatePaymentRequest(PaymentInterface $payment, array $form) {
    /** @var \Drupal\address\AddressInterface $billing_address */
    if ($payment->getOrder()->getBillingProfile()) {
      $billing_address = $payment->getOrder()->getBillingProfile()->get('address')->first();
    }
    if (empty($billing_address)) {
      throw new HardDeclineException('No billing address defined for the payment.');
    }

    $use_preauthorization = empty($form['#capture']);
    $request = $this->getBasicServerApiAuthorizationParameters($payment, $billing_address, $use_preauthorization);
    $request['successurl'] = $this->buildReturnUrl($form['#return_url'], $payment->getOrderId())->toString();
    $request['backurl'] = $form['#cancel_url'];
    $request['errorurl'] = $form['#cancel_url'];
    $request = array_merge($request, $this->getCartItemParameters($payment));
    $this->populateAuthorizationRequest($request, $payment, $billing_address);

    // Allow modules to adjust the request.
    $event = new PayoneRequestEvent($payment, $request);
    $event_name = $use_preauthorization ? CommercePayoneEvents::PAYONE_PREAUTHORIZATION_REQUEST : CommercePayoneEvents::PAYONE_AUTHORIZATION_REQUEST;
    $this->eventDispatcher->dispatch($event_name, $event);
    $request = $event->getRequest();

    return $this->api->processHttpPost($request, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);

    $payment_gateway_data = $order->getData($this->getOrderDataKey());
    if (empty($payment_gateway_data['transaction_id'])) {
      throw new InvalidRequestException('Transaction ID missing for this transaction.');
    }

    $transaction_id = $payment_gateway_data['transaction_id'];
    $payment = $this->paymentStorage->loadByRemoteId($transaction_id);
    if (empty($payment)) {
      throw new InvalidRequestException('No transaction found for transaction ID @transaction_id.', ['@transaction_id' => $transaction_id]);
    }

    $payment->state = 'authorization_voided';
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    try {
      // Perform the capture request here, throw an exception if it fails.
      // See \Drupal\commerce_payment\Exception for the available exceptions.
      $response = $this->requestCapture($payment, $amount);
      ErrorHelper::handleErrors($response);
      if (!empty($response->status)) {
        $payment->setRemoteState($response->status);
      }
    }
    catch (\Exception $e) {
      ErrorHelper::handleException($e);
    }

    // Update the local payment entity.
    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
    // Also save the order entity, because we have modified the 'data' field.
    $payment->getOrder()->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    $amount = new Price('0', $payment->getAmount()->getCurrencyCode());

    try {
      // Perform the capture request here, throw an exception if it fails.
      // See \Drupal\commerce_payment\Exception for the available exceptions.
      $response = $this->requestCapture($payment, $amount);
      ErrorHelper::handleErrors($response);
      if (!empty($response->status)) {
        $payment->setRemoteState($response->status);
      }
    }
    catch (\Exception $e) {
      ErrorHelper::handleException($e);
    }

    // Update the local payment entity.
    $payment->setState('authorization_voided');
    $payment->save();
    // Also save the order entity, because we have modified the 'data' field.
    $payment->getOrder()->save();
  }

  /**
   * Performs the capture request for the given payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param \Drupal\commerce_price\Price|null $amount
   *   The amount to capture. If NULL, defaults to the entire payment amount.
   *
   * @return object
   *   The API response as JSON array.
   */
  protected function requestCapture(PaymentInterface $payment, Price $amount) {
    $order = $payment->getOrder();
    $request = $this->api->getServerApiStandardParameters($this->configuration, 'capture');
    $request['amount'] = $this->toMinorUnits($amount);
    $request['currency'] = $payment->getAmount()->getCurrencyCode();
    $request['txid'] = $payment->getRemoteId();
    $request['sequencenumber'] = $this->getNextSequenceNumber($order);

    // Allow modules to adjust the request.
    $event = new PayoneRequestEvent($payment, $request);
    $this->eventDispatcher->dispatch(CommercePayoneEvents::PAYONE_CAPTURE_REQUEST, $event);
    $request = $event->getRequest();

    return $this->api->processHttpPost($request, FALSE);
  }

  /**
   * Returns an array populated with basic authorization request data.
   *
   * The array already includes the call to getServerApiStandardParameters().
   *
   * Payment method specific additional data like wallettype must be populated
   * in the payment gateway itself.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param \Drupal\address\AddressInterface $billing_address
   *   The billing address.
   * @param bool $use_preauthorization
   *   Whether to use 'preauthorization' request rather than 'authorization'.
   *   Defaults to FALSE.
   *
   * @return array
   *   An array populated with basic data needed for any payment type.
   */
  protected function getBasicServerApiAuthorizationParameters(PaymentInterface $payment, AddressInterface $billing_address, $use_preauthorization = FALSE) {
    $order = $payment->getOrder();
    $owner = $order->getCustomer();
    if ($owner && $owner->isAuthenticated()) {
      $customer_id = $this->getRemoteCustomerId($owner);
      $customer_email = $owner->getEmail();
    }
    else {
      $customer_email = $order->getEmail();
    }

    $parameters = [
      'clearingtype' => $this->getClearingType(),
      'aid' => $this->configuration['sub_account_id'],
      // Reference must be unique.
      'reference' => $payment->getOrderId() . '_' . $this->time->getCurrentTime(),
      'amount' => $this->toMinorUnits($payment->getAmount()),
      'currency' => $payment->getAmount()->getCurrencyCode(),
      'email' => $customer_email,
      'firstname' => $billing_address->getGivenName(),
      'lastname' => $billing_address->getFamilyName(),
      'company' => $billing_address->getOrganization(),
      'street' => $billing_address->getAddressLine1(),
      'addressaddition' => $billing_address->getAddressLine2(),
      'zip' => $billing_address->getPostalCode(),
      'city' => $billing_address->getLocality(),
      'country' => $billing_address->getCountryCode(),
      'ip' => $order->getIpAddress(),
    ];

    if (!empty($customer_id)) {
      $parameters['userid'] = $customer_id;
    }

    if ($owner && $owner->isAuthenticated()) {
      $parameters['customerid'] = $owner->id();
    }

    if ($shipping_profile = $this->getShippingProfile($order)) {
      if ($shipping_address = $shipping_profile->get('address')->first()) {
        $parameters['shipping_firstname'] = $shipping_address->getGivenName();
        $parameters['shipping_lastname'] = $shipping_address->getFamilyName();
        $parameters['shipping_company'] = $shipping_address->getOrganization();
        $parameters['shipping_street'] = $shipping_address->getAddressLine1();
        $parameters['shipping_addressaddition'] = $shipping_address->getAddressLine2();
        $parameters['shipping_zip'] = $shipping_address->getPostalCode();
        $parameters['shipping_city'] = $shipping_address->getLocality();
        $parameters['shipping_country'] = $shipping_address->getCountryCode();
      }
    }

    $request_method = $use_preauthorization ? 'preauthorization' : 'authorization';
    return array_merge($this->api->getServerApiStandardParameters($this->configuration, $request_method), $parameters);
  }

  /**
   * Get payment redirect key.
   *
   * @param int $order_id
   *   The order id.
   *
   * @return string
   *   The redirect key.
   */
  protected function getPaymentRedirectKey(int $order_id) {
    $data = $this->api->getServerApiStandardParameters($this->configuration, 'authorization');
    $data['order_id'] = $order_id;
    return $this->api->generateHash($data, $this->configuration['key'], $this->configuration['hash_method']);
  }

  /**
   * Build the return URL including hashed 'key' parameter.
   *
   * @param string $url
   *   The basic url.
   * @param int $order_id
   *   The order id.
   *
   * @return \Drupal\Core\Url
   *   The url.
   */
  protected function buildReturnUrl(string $url, int $order_id) {
    return Url::fromUri($url, [
      'absolute' => TRUE,
      'query' => ['key' => $this->getPaymentRedirectKey($order_id)],
    ]);
  }

  /**
   * Gets the shipping profile from the given order, if available.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\profile\Entity\ProfileInterface|null
   *   The shipping profile, if available - NULL otherwise.
   */
  protected function getShippingProfile(OrderInterface $order) {
    if ($order->hasField('shipments')) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      foreach ($order->shipments->referencedEntities() as $shipment) {
        return $shipment->getShippingProfile();
      }
    }

    return NULL;
  }

  /**
   * Populates the already with standard parameters prefilled request data.
   *
   * The request data already contains the standard Server API parameters, as
   * well as any default payment related data, such as amount, order reference,
   * address data, and many more.
   *
   * The implementations only need to set their individual parameters such as
   * wallettype.
   *
   * Please note that the underlying request may either be an "authorization"
   * or a "preauthorization" one - depending on the configuration setting in
   * the underlying Commerce checkout flow.
   *
   * @param array $request
   *   The request data.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param \Drupal\address\AddressInterface $billing_address
   *   The billing address.
   */
  abstract protected function populateAuthorizationRequest(array &$request, PaymentInterface $payment, AddressInterface $billing_address);

  /**
   * Get the clearing type of this plugin.
   *
   * @return string
   *   The clearing type of this plugin to be used for API requests.
   */
  abstract protected function getClearingType();

}
