<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

/**
 * Provides the interface for the PayPal payment gateway.
 */
interface PayonePaypalInterface extends PayoneOffsitePaymentGatewayInterface {

}
