<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Provides the On-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "payone_sepa",
 *   label = "Payone SEPA (On-site)",
 *   display_label = "SEPA",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_payone\PluginForm\Sepa\SepaPaymentMethodAddForm",
 *   },
 *   payment_method_types = {"commerce_payone_sepa"},
 *   requires_billing_information = FALSE,
 * )
 */
class PayoneSepa extends PayoneOnsitePaymentGatewayBase implements PayoneSepaInterface {

  /**
   * {@inheritdoc}
   */
  protected function getClearingType() {
    return 'elv';
  }

  /**
   * {@inheritdoc}
   */
  protected function populatePreauthorizationRequest(array &$request, PaymentInterface $payment) {
    $payment_method = $payment->getPaymentMethod();
    $request['iban'] = $payment_method->iban->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderDataKey() {
    return 'payone_sepa';
  }

}
