<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the On-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "payone_invoice",
 *   label = "Payone Invoice (On-site)",
 *   display_label = "Invoice",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_payone\PluginForm\Invoice\InvoicePaymentMethodAddForm",
 *   },
 *   payment_method_types = {"commerce_payone_invoice"},
 *   requires_billing_information = FALSE,
 * )
 */
class PayoneInvoice extends PayoneOnsitePaymentGatewayBase implements PayoneInvoiceInterface {

  /**
   * The clearing sub type for secure invoices in pre-authorization requests.
   */
  const SECURE_INVOICE_SUBTYPE = 'POV';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'payone_businessrelation' => 'b2c',
      'payone_secure_invoice' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['payone_businessrelation'] = [
      '#type' => 'select',
      '#title' => $this->t('Business Relation'),
      '#options' => [
        'b2c' => $this->t('B2C'),
        'b2b' => $this->t('B2B'),
      ],
      '#default_value' => $this->configuration['payone_businessrelation'],
      '#required' => TRUE,
    ];

    $form['payone_secure_invoice'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Payone Secure invoice'),
      '#default_value' => $this->configuration['payone_secure_invoice'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['payone_businessrelation'] = $values['payone_businessrelation'];
      $this->configuration['payone_secure_invoice'] = $values['payone_secure_invoice'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details = []) {
    // Do not allow reuse for the payment method.
    $payment_method->setReusable(FALSE);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getClearingType() {
    return 'rec';
  }

  /**
   * {@inheritdoc}
   */
  protected function populatePreauthorizationRequest(array &$request, PaymentInterface $payment) {
    if (isset($this->configuration['payone_secure_invoice'])
      && $this->configuration['payone_secure_invoice']) {
      $request['clearingsubtype'] = self::SECURE_INVOICE_SUBTYPE;
    }
    if (isset($this->configuration['payone_businessrelation'])
      && $this->configuration['payone_businessrelation']) {
      $request['businessrelation'] = $this->configuration['payone_businessrelation'];
    }

    $payment_method = $payment->getPaymentMethod();
    if ($payment_method && !$payment_method->get('dob')->isEmpty()) {
      /** @var \Drupal\Core\Datetime\DrupalDateTime|null $date */
      if ($date = $payment_method->get('dob')->first()->get('date')->getValue()) {
        $request['birthday'] = $date->format('Ymd');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function populateCaptureRequest(array &$request, PaymentInterface $payment) {
    if ($order_number = $payment->getOrder()->getOrderNumber()) {
      $request['invoiceid'] = $order_number;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function sendCartItemsOnCapture() {
    return !empty($this->configuration['payone_secure_invoice']);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderDataKey() {
    return 'payone_invoice';
  }

}
