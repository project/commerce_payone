<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payone\ErrorHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the On-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "payone_cc",
 *   label = "Payone Credit Card (On-site)",
 *   display_label = "Payone Credit Card",
 *    forms = {
 *     "add-payment-method" = "Drupal\commerce_payone\PluginForm\CreditCard\CreditCardPaymentMethodAddForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   js_library = "commerce_payone/form",
 *   requires_billing_information = FALSE,
 * )
 */
class PayoneCreditCard extends PayoneOnsitePaymentGatewayBase implements PayoneCreditCardInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'allowed_cards' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $card_type_map = array_flip(self::creditCardMap());
    $credit_card_types = CreditCard::getTypeLabels();
    $form['allowed_cards'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => array_intersect_key($credit_card_types, $card_type_map),
      '#title' => $this->t('Allowed credit card types'),
      '#default_value' => $this->configuration['allowed_cards'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['allowed_cards'] = $values['allowed_cards'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $billing_address */
    if ($billing_address = $payment_method->getBillingProfile()) {
      $billing_address = $payment_method->getBillingProfile()->get('address')->first();
    }
    else {
      throw new HardDeclineException('No billing address defined for the payment');
    }

    // Preauthorize payment.
    try {
      $response = $this->requestPreauthorization($payment, $billing_address);
      ErrorHelper::handleErrors($response);
    }
    catch (\Exception $e) {
      ErrorHelper::handleException($e);
      // Return to satisfy static code analyzers, although it ain't needed, as
      // ErrorHandler::handleException always throws an exception.
      return;
    }

    // Update the local payment entity.
    $payment->setState('preauthorization');
    $payment->setRemoteId($response->txid);
    $payment->save();

    $owner = $payment_method->getOwner();
    if ($owner && $owner->isAuthenticated()) {
      $this->setRemoteCustomerId($owner, $response->userid);
      $owner->save();
    }

    // 3-D Secure check if status 'REDIRECT'.
    if ($response->status == 'REDIRECT') {
      $payment->getOrder()->setData($this->getOrderDataKey(), [
        'transaction_id' => $response->txid,
      ])->save();
      throw new NeedsRedirectException($response->redirecturl);
    }

    // Capture payment.
    if ($capture) {
      try {
        $response = $this->requestCapture($payment);
        ErrorHelper::handleErrors($response);
        if (!empty($response->status)) {
          $payment->setRemoteState($response->status);
        }
      }
      catch (\Exception $e) {
        ErrorHelper::handleException($e);
      }
    }

    $payment_state = $capture ? 'completed' : 'authorization';
    $payment->setState($payment_state);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the PaymentMethodAddForm form elements. They are expected to be valid.
      'pseudocardpan',
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }
    // Perform the create request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // You might need to do different API requests based on whether the
    // payment method is reusable: $payment_method->isReusable().
    // Non-reusable payment methods usually have an expiration timestamp.
    $payment_method->card_type = $this->mapCreditCardType($payment_details['cardtype']);
    // Only the last 4 numbers are safe to store.
    $payment_method->card_number = substr($payment_details['truncatedcardpan'], -4);
    $payment_method->card_exp_month = substr($payment_details['cardexpiredate'], -2);
    $payment_method->card_exp_year = substr(date('Y'), 0, 2) . substr($payment_details['cardexpiredate'], 0, 2);
    $expires = CreditCard::calculateExpirationTimestamp($payment_method->card_exp_month->value, $payment_method->card_exp_year->value);

    // The remote ID returned by the request.
    $remote_id = $payment_details['pseudocardpan'];

    $owner = $payment_method->getOwner();
    if ($owner && $owner->isAuthenticated() && !empty($payment_details['userid'])) {
      $this->setRemoteCustomerId($owner, $payment_details['userid']);
      $owner->save();
    }

    $payment_method->setRemoteId($remote_id);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();
  }

  /**
   * Maps the Payone credit card type to a Commerce credit card type.
   *
   * @param string $card_type
   *   The Payone credit card type.
   *
   * @return string
   *   The Commerce credit card type.
   */
  protected function mapCreditCardType($card_type) {
    $map = self::creditCardMap();
    if (!isset($map[$card_type])) {
      throw new HardDeclineException(sprintf('Unsupported credit card type "%s".', $card_type));
    }

    return $map[$card_type];
  }

  /**
   * Simple map of credit card keys.
   */
  public static function creditCardMap() {
    return [
      'V' => 'visa',
      'M' => 'mastercard',
      'A' => 'amex',
      'D' => 'dinersclub',
      'J' => 'jcb',
      'O' => 'maestro',
      'U' => 'maestro',
      'C' => 'discover',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getClearingType() {
    return 'cc';
  }

  /**
   * {@inheritdoc}
   */
  protected function populatePreauthorizationRequest(array &$request, PaymentInterface $payment) {
    $request['successurl'] = $this->buildSecurityCheckReturnUrl($payment->getOrderId());
    $request['errorurl'] = $this->buildSecurityCheckCancelUrl($payment->getOrderId());
    $request['backurl'] = $request['errorurl'];
    $request['hash'] = $this->api->generateHash($request, $this->configuration['key'], $this->configuration['hash_method']);
    $request['pseudocardpan'] = $payment->getPaymentMethod()->getRemoteId();
  }

  /**
   * {@inheritdoc}
   */
  public function onSecurityCheckReturn(OrderInterface $order, Request $request, $capture = TRUE) {
    if (empty($request->query->get('key')) || $request->query->get('key') !== $this->getPaymentRedirectKey($order->id())) {
      throw new PaymentGatewayException('Payment could not be authorized.');
    }

    $payment = $this->getPayment($order);
    if (empty($payment)) {
      throw new PaymentGatewayException('Invalid order without Payone credit card payment.');
    }

    if ($capture) {
      $this->capturePayment($payment);
    }

    $payment_state = $capture ? 'completed' : 'authorization';
    $payment->setState($payment_state);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function onSecurityCheckCancel(OrderInterface $order) {
    $this->messenger()->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
      '@gateway' => $this->getDisplayLabel(),
    ]));
  }

  /**
   * Gets the Payone credit card payment from the given order entity.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|null
   *   The credit card payment, or NULL if not found.
   */
  protected function getPayment(OrderInterface $order) {
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    $payment_gateway_data = $order->getData('payone_cc');
    $transaction_id = !empty($payment_gateway_data) ? $payment_gateway_data['transaction_id'] : NULL;
    if (!empty($transaction_id)) {
      return $payment_storage->loadByRemoteId($transaction_id);
    }
    return NULL;
  }

  /**
   * Builds the URL to the "return" page.
   *
   * @param int $order_id
   *   The order id.
   *
   * @return string
   *   The "return" page url.
   */
  protected function buildSecurityCheckReturnUrl(int $order_id) {
    return Url::fromRoute('commerce_payone.3ds.return', [
      'commerce_order' => $order_id,
      'step' => 'payment',
    ], ['absolute' => TRUE, 'query' => ['key' => $this->getPaymentRedirectKey($order_id)]])->toString();
  }

  /**
   * Builds the URL to the "cancel" page.
   *
   * @param int $order_id
   *   The order id.
   *
   * @return string
   *   The "cancel" page url.
   */
  protected function buildSecurityCheckCancelUrl(int $order_id) {
    return Url::fromRoute('commerce_payone.3ds.cancel', [
      'commerce_order' => $order_id,
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();
  }

  /**
   * Generate payment redirect key.
   *
   * @param int $order_id
   *   The order id.
   *
   * @return string
   *   The redirect key.
   */
  protected function getPaymentRedirectKey(int $order_id) {
    $data = $this->api->getServerApiStandardParameters($this->configuration, 'preauthorization');
    $data['order_id'] = $order_id;
    return $this->api->generateHash($data, $this->configuration['key'], $this->configuration['hash_method']);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderDataKey() {
    return 'payone_cc';
  }

}
