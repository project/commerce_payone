<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

use Drupal\address\AddressInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "payone_paypal",
 *   label = "Payone PayPal (Off-site)",
 *   display_label = "Payone PayPal",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_payone\PluginForm\Paypal\PaypalOffsiteForm",
 *   },
 *   payment_method_types = {"commerce_payone_paypal"},
 *   requires_billing_information = FALSE,
 * )
 */
class PayonePaypal extends PayoneOffsitePaymentGatewayBase implements PayonePaypalInterface {

  /**
   * {@inheritdoc}
   */
  protected function getClearingType() {
    return 'wlt';
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderDataKey() {
    return 'payone_paypal';
  }

  /**
   * {@inheritdoc}
   */
  protected function populateAuthorizationRequest(array &$request, PaymentInterface $payment, AddressInterface $billing_address) {
    $request['wallettype'] = 'PPE';
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_gateway_data = $order->getData($this->getOrderDataKey());
    $transaction_id = $payment_gateway_data['transaction_id'];
    $capture = $payment_gateway_data['capture'];

    if (empty($transaction_id)) {
      throw new PaymentGatewayException('Transaction ID missing for transaction.');
    }

    if (empty($payment_gateway_data['token'])) {
      throw new PaymentGatewayException('Token data missing for PayPal transaction.');
    }

    if (empty($request->query->get('key')) || $request->query->get('key') !== $this->getPaymentRedirectKey($order->id())) {
      throw new PaymentGatewayException('Payment could not be authorized.');
    }

    $payment = $this->paymentStorage->loadByRemoteId($transaction_id);
    if (empty($payment)) {
      $this->getLogger('commerce_payone')->warning('No payment found for remote ID @transaction_id. A new payment entity will be created instead.', ['@transaction_id' => $transaction_id]);

      $payment = $this->paymentStorage->create([
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->parentEntity->id(),
        'order_id' => $order->id(),
        'remote_id' => $transaction_id,
      ]);
    }
    $payment_state = $capture ? 'completed' : 'authorization';
    $payment->setState($payment_state);
    $payment->setRemoteState('APPROVED');
    $payment->save();
  }

}
