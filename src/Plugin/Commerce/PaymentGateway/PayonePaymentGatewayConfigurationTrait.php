<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payone\HashGenerator;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a trait for common Payone configuration handling.
 */
trait PayonePaymentGatewayConfigurationTrait {

  /**
   * Gets default Payone payment gateway configuration.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  public function defaultPayonePaymentGatewayConfiguration() {
    return [
      'merchant_id' => '',
      'portal_id' => '',
      'sub_account_id' => '',
      'key' => '',
      'hash_method' => HashGenerator::HASH_METHOD_MD5,
      'use_debit_for_refund' => TRUE,
    ];
  }

  /**
   * Builds the common Payone payment gateway configuration form.
   *
   * @param array $form
   *   An associative array containing the initial structure of the plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form. Calling code should pass on a subform
   *   state created through
   *   \Drupal\Core\Form\SubformState::createForSubform().
   *
   * @return array
   *   The form structure.
   */
  public function buildPayonePaymentGatewayConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    $form['portal_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Portal ID'),
      '#default_value' => $this->configuration['portal_id'],
      '#required' => TRUE,
    ];

    $form['sub_account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sub account ID'),
      '#default_value' => $this->configuration['sub_account_id'],
      '#required' => TRUE,
    ];

    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#default_value' => $this->configuration['key'],
      '#required' => TRUE,
    ];

    $form['hash_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Hash method'),
      '#options' => [
        HashGenerator::HASH_METHOD_MD5 => HashGenerator::HASH_METHOD_MD5,
        HashGenerator::HASH_METHOD_SHA2_384 => HashGenerator::HASH_METHOD_SHA2_384,
      ],
      '#default_value' => $this->configuration['hash_method'],
      '#required' => TRUE,
    ];

    $form['use_debit_for_refund'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use debit request for refunds'),
      '#description' => $this->t('The "debit" request can be used instead of the "refund" request. The "debit" request is only available with PAYONE Business. For Payone secure invoice, only the "debit" request works for refunding payments that the buyer has not paid yet to Payone.'),
      '#default_value' => $this->configuration['use_debit_for_refund'],
    ];

    return $form;
  }

  /**
   * Sets the submitted common Payone configuration options.
   *
   * To be used within form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the plugin form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $configuration
   *   The configuration array to modify.
   */
  public function submitPayonePaymentGatewayConfigurationForm(array &$form, FormStateInterface $form_state, array &$configuration) {
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $configuration['merchant_id'] = $values['merchant_id'];
      $configuration['portal_id'] = $values['portal_id'];
      $configuration['sub_account_id'] = $values['sub_account_id'];
      $configuration['key'] = $values['key'];
      $configuration['hash_method'] = $values['hash_method'];
      $configuration['use_debit_for_refund'] = $values['use_debit_for_refund'];
    }
  }

}
