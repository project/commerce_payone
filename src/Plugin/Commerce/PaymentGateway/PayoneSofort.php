<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

use Drupal\address\AddressInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "payone_sofort",
 *   label = "Payone Sofortüberweisung (Off-site)",
 *   display_label = "Payone Sofortüberweisung",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_payone\PluginForm\SofortBanking\SofortBankingOffsiteForm",
 *   },
 *   payment_method_types = {"commerce_payone_sofort"},
 *   requires_billing_information = FALSE,
 * )
 */
class PayoneSofort extends PayoneOffsitePaymentGatewayBase implements PayoneSofortInterface {

  /**
   * {@inheritdoc}
   */
  protected function getClearingType() {
    return 'sb';
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderDataKey() {
    return 'payone_sofort';
  }

  /**
   * {@inheritdoc}
   */
  protected function populateAuthorizationRequest(array &$request, PaymentInterface $payment, AddressInterface $billing_address) {
    $request['onlinebanktransfertype'] = 'PNT';
    $request['bankcountry'] = $billing_address->getCountryCode();
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_gateway_data = $order->getData($this->getOrderDataKey());
    $transaction_id = $payment_gateway_data['transaction_id'];
    $capture = $payment_gateway_data['capture'];

    if (empty($transaction_id)) {
      throw new PaymentGatewayException('Transaction ID missing for transaction.');
    }

    if (empty($request->query->get('key')) || $request->query->get('key') !== $this->getPaymentRedirectKey($order->id())) {
      throw new PaymentGatewayException('Payment could not be authorized.');
    }

    $payment = $this->paymentStorage->loadByRemoteId($transaction_id);
    if (empty($payment)) {
      $this->getLogger('commerce_payone')->warning('No payment found for remote ID @transaction_id. A new payment entity will be created instead.', ['@transaction_id' => $transaction_id]);

      $payment = $this->paymentStorage->create([
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->parentEntity->id(),
        'order_id' => $order->id(),
        'remote_id' => $transaction_id,
      ]);
    }
    $payment_state = $capture ? 'completed' : 'authorization';
    $payment->setState($payment_state);
    $payment->setRemoteState('APPROVED');
    $payment->save();
  }

}
