<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

/**
 * Provides the interface for the SOFORT payment gateway.
 */
interface PayoneSofortInterface extends PayoneOffsitePaymentGatewayInterface {

}
