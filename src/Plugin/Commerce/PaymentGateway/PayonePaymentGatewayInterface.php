<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

/**
 * Defines a common interface for Payone payment gateways.
 */
interface PayonePaymentGatewayInterface {

  /**
   * Gets the key to be used to store Gateway specific info in the order data.
   *
   * @return string
   *   The key to be used to store Gateway specific info in the order data.
   */
  public function getOrderDataKey();

}
