<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

use Drupal\address\AddressInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;
use Drupal\commerce_payone\ErrorHelper;
use Drupal\commerce_payone\Event\CommercePayoneEvents;
use Drupal\commerce_payone\Event\PayoneRequestEvent;
use Drupal\commerce_payone\PayoneApiServiceInterface;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Abstract base class for any Payone onsite payment gateway.
 */
abstract class PayoneOnsitePaymentGatewayBase extends OnsitePaymentGatewayBase implements PayonePaymentGatewayInterface, SupportsRefundsInterface, SupportsVoidsInterface {

  use PayonePaymentGatewayConfigurationTrait;
  use PayonePaymentGatewayTrait;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a new PayoneOnsitePaymentGatewayBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   * @param \Drupal\commerce_payone\PayoneApiServiceInterface $api_service
   *   The Payone API service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    MinorUnitsConverterInterface $minor_units_converter,
    PayoneApiServiceInterface $api_service,
    EventDispatcherInterface $event_dispatcher
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);

    $this->api = $api_service;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('commerce_payone.payment_api'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return $this->defaultPayonePaymentGatewayConfiguration()
      + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    return $this->buildPayonePaymentGatewayConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->submitPayonePaymentGatewayConfigurationForm($form, $form_state, $this->configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    /** @var \Drupal\address\AddressInterface $billing_address */
    if ($payment_method->getBillingProfile()) {
      $billing_address = $payment_method->getBillingProfile()->get('address')->first();
    }
    else {
      throw new HardDeclineException('No billing address defined for the payment.');
    }

    // Preauthorize payment.
    try {
      $response = $this->requestPreauthorization($payment, $billing_address);
      ErrorHelper::handleErrors($response);
    }
    catch (\Exception $e) {
      ErrorHelper::handleException($e);
      // Return to satisfy static code analyzers, although it ain't needed, as
      // ErrorHandler::handleException always throws an exception.
      return;
    }

    // Update the local payment entity.
    $payment->setState('preauthorization');
    $payment->setRemoteId($response->txid);
    $payment->save();

    $owner = $payment_method->getOwner();
    if ($owner && $owner->isAuthenticated()) {
      $this->setRemoteCustomerId($owner, $response->userid);
      $owner->save();
    }

    // Perform capture.
    if ($capture) {
      try {
        $response = $this->requestCapture($payment);
        ErrorHelper::handleErrors($response);
        if (!empty($response->status)) {
          $payment->setRemoteState($response->status);
        }
      }
      catch (\Exception $e) {
        ErrorHelper::handleException($e);
      }
    }

    $payment_state = $capture ? 'completed' : 'authorization';
    $payment->setState($payment_state);
    $payment->save();
    if ($capture) {
      // Also save the order entity, because we have modified the 'data' field.
      $payment->getOrder()->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['preauthorization', 'authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    try {
      // Perform the capture request here, throw an exception if it fails.
      // See \Drupal\commerce_payment\Exception for the available exceptions.
      $response = $this->requestCapture($payment, $amount);
      ErrorHelper::handleErrors($response);
      if (!empty($response->status)) {
        $payment->setRemoteState($response->status);
      }
    }
    catch (\Exception $e) {
      ErrorHelper::handleException($e);
    }

    // Update the local payment entity.
    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    $amount = new Price('0', $payment->getAmount()->getCurrencyCode());

    try {
      // Perform the capture request here, throw an exception if it fails.
      // See \Drupal\commerce_payment\Exception for the available exceptions.
      $response = $this->requestCapture($payment, $amount);
      ErrorHelper::handleErrors($response);
      if (!empty($response->status)) {
        $payment->setRemoteState($response->status);
      }
    }
    catch (\Exception $e) {
      ErrorHelper::handleException($e);
    }

    // Update the local payment entity.
    $payment->setState('authorization_voided');
    $payment->save();
    // Also save the order entity, because we have modified the 'data' field.
    $payment->getOrder()->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $owner = $payment_method->getOwner();
    if ($owner && $owner->isAuthenticated() && !empty($payment_details['userid'])) {
      $this->setRemoteCustomerId($owner, $payment_details['userid']);
      $owner->save();
    }

    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the local entity only, as there's no call in Payone API available
    // to delete a remote payment method - at least not obviously.
    $payment_method->delete();
  }

  /**
   * Performs the capture request for the given payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param \Drupal\commerce_price\Price|null $amount
   *   The amount to capture. If NULL, defaults to the entire payment amount.
   *
   * @return object
   *   The API response as JSON array.
   */
  final protected function requestCapture(PaymentInterface $payment, Price $amount = NULL) {
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $order = $payment->getOrder();
    $request = $this->api->getServerApiStandardParameters($this->configuration, 'capture');
    $request['amount'] = $this->toMinorUnits($amount);
    $request['currency'] = $payment->getAmount()->getCurrencyCode();
    $request['txid'] = $payment->getRemoteId();
    $request['sequencenumber'] = $this->getNextSequenceNumber($order);
    if ($this->sendCartItemsOnCapture()) {
      $request = array_merge($request, $this->getCartItemParameters($payment, $amount));
    }
    $this->populateCaptureRequest($request, $payment);

    // Allow modules to adjust the request.
    $event = new PayoneRequestEvent($payment, $request);
    $this->eventDispatcher->dispatch(CommercePayoneEvents::PAYONE_CAPTURE_REQUEST, $event);
    $request = $event->getRequest();

    return $this->api->processHttpPost($request, FALSE);
  }

  /**
   * Sends a preauthorization request for the given payment and address.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param \Drupal\address\AddressInterface $address
   *   The billing address belonging to the payment.
   *
   * @return object
   *   The decoded JSON response from the API call.
   */
  final protected function requestPreauthorization(PaymentInterface $payment, AddressInterface $address) {
    $request = $this->api->getServerApiStandardParameters($this->configuration, 'preauthorization');
    $request = array_merge($request, $this->getBasicServerApiPreauthorizationParameters($payment, $address));
    $request = array_merge($request, $this->getCartItemParameters($payment));
    $this->populatePreauthorizationRequest($request, $payment);

    // Allow modules to adjust the request.
    $event = new PayoneRequestEvent($payment, $request);
    $this->eventDispatcher->dispatch(CommercePayoneEvents::PAYONE_PREAUTHORIZATION_REQUEST, $event);
    $request = $event->getRequest();

    return $this->api->processHttpPost($request, FALSE);
  }

  /**
   * Returns an array populated with basic data needed for any payment type.
   *
   * Payment method specific data like optional clearing subtype, as well as
   * any specific additional fields (like birthday) must be populated in the
   * payment gateway plugin implementations. The data returned by this function
   * can and should be used by both pre-authorization and capture calls.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param \Drupal\address\AddressInterface $billing_address
   *   The billing address.
   *
   * @return array
   *   An array populated with basic data needed for any payment type.
   */
  protected function getBasicServerApiPreauthorizationParameters(PaymentInterface $payment, AddressInterface $billing_address) {
    $order = $payment->getOrder();
    $payment_method = $payment->getPaymentMethod();

    $owner = $payment_method->getOwner();
    if ($owner && $owner->isAuthenticated()) {
      $customer_id = $this->getRemoteCustomerId($owner);
      $customer_email = $owner->getEmail();
    }
    else {
      $customer_email = $order->getEmail();
    }

    $parameters = [
      'clearingtype' => $this->getClearingType(),
      'aid' => $this->configuration['sub_account_id'],
      // Reference must be unique.
      'reference' => $payment->getOrderId() . '_' . $this->time->getCurrentTime(),
      'amount' => $this->toMinorUnits($payment->getAmount()),
      'currency' => $payment->getAmount()->getCurrencyCode(),
      'email' => $customer_email,
      'firstname' => $billing_address->getGivenName(),
      'lastname' => $billing_address->getFamilyName(),
      'company' => $billing_address->getOrganization(),
      'street' => $billing_address->getAddressLine1(),
      'addressaddition' => $billing_address->getAddressLine2(),
      'zip' => $billing_address->getPostalCode(),
      'city' => $billing_address->getLocality(),
      'country' => $billing_address->getCountryCode(),
      'ip' => $order->getIpAddress(),
    ];

    if (!empty($customer_id)) {
      $parameters['userid'] = $customer_id;
    }

    if ($owner && $owner->isAuthenticated()) {
      $parameters['customerid'] = $owner->id();
    }

    if ($shipping_profile = $this->getShippingProfile($order)) {
      if ($shipping_address = $shipping_profile->get('address')->first()) {
        $parameters['shipping_firstname'] = $shipping_address->getGivenName();
        $parameters['shipping_lastname'] = $shipping_address->getFamilyName();
        $parameters['shipping_company'] = $shipping_address->getOrganization();
        $parameters['shipping_street'] = $shipping_address->getAddressLine1();
        $parameters['shipping_addressaddition'] = $shipping_address->getAddressLine2();
        $parameters['shipping_zip'] = $shipping_address->getPostalCode();
        $parameters['shipping_city'] = $shipping_address->getLocality();
        $parameters['shipping_country'] = $shipping_address->getCountryCode();
      }
    }

    return $parameters;
  }

  /**
   * Gets the shipping profile from the given order, if available.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\profile\Entity\ProfileInterface|null
   *   The shipping profile, if available - NULL otherwise.
   */
  protected function getShippingProfile(OrderInterface $order) {
    if ($order->hasField('shipments')) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      foreach ($order->shipments->referencedEntities() as $shipment) {
        return $shipment->getShippingProfile();
      }
    }

    return NULL;
  }

  /**
   * Returns whether to send cart item parameters with capture request.
   *
   * @return bool
   *   TRUE, if cart item parameters should be send with capture requests, FALSE
   *   otherwise. This defaults to FALSE and will only be included for plugins
   *   needing it (Invoice).
   */
  protected function sendCartItemsOnCapture() {
    return FALSE;
  }

  /**
   * Populates the already with standard parameters prefilled request data.
   *
   * The request data already contains the standard Server API parameters, as
   * well as any default payment related data, such as amount, order reference,
   * address data, and many more.
   *
   * The implementations only need to set their individual parameters such as
   * clearing type, as well as any additional specific fields (eg. birthday).
   *
   * @param array $request
   *   The request data.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   */
  abstract protected function populatePreauthorizationRequest(array &$request, PaymentInterface $payment);

  /**
   * Populates the already with standard parameters prefilled capture request.
   *
   * The request data already contains the standard Server API parameters, as
   * well as any default payment related data, such as amount, order reference,
   * address data, and many more.
   *
   * The implementations can enrich the data with payment method specific
   * default data, such as 'invoiceid' for invoice payments.
   *
   * @param array $request
   *   The request data.
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   */
  protected function populateCaptureRequest(array &$request, PaymentInterface $payment) {
  }

  /**
   * Get the clearing type of this plugin.
   *
   * @return string
   *   The clearing type of this plugin to be used for API requests.
   */
  abstract protected function getClearingType();

}
