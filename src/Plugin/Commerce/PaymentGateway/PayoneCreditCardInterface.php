<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the interface for the Payone credit card payment gateway.
 */
interface PayoneCreditCardInterface extends OnsitePaymentGatewayInterface, SupportsAuthorizationsInterface, SupportsRefundsInterface {

  /**
   * Processes the "return" request for 3-D Secure check.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param bool $capture
   *   Whether the created payment should be captured (VS authorized only).
   */
  public function onSecurityCheckReturn(OrderInterface $order, Request $request, $capture = TRUE);

  /**
   * Processes the "cancel" request for 3-D Secure check.
   *
   * Allows the payment gateway to clean up any data added to the $order, set
   * a message for the customer.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function onSecurityCheckCancel(OrderInterface $order);

}
