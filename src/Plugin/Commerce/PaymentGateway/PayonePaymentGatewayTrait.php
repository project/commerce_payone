<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payone\ErrorHelper;
use Drupal\commerce_payone\Event\CommercePayoneEvents;
use Drupal\commerce_payone\Event\PayoneCartItemParametersEvent;
use Drupal\commerce_payone\Event\PayoneRefundEvent;
use Drupal\commerce_payone\Event\PayoneRequestEvent;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Provides common functionality for both onsite and offsite plugins.
 */
trait PayonePaymentGatewayTrait {

  /**
   * The gateway used for making API calls.
   *
   * @var \Drupal\commerce_payone\PayoneApiServiceInterface
   */
  protected $api;

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    // Validate the requested amount.
    $this->assertRefundAmount($payment, $amount);

    try {
      if ($this->configuration['use_debit_for_refund']) {
        $response = $this->requestDebit($payment, $amount);
      }
      else {
        $response = $this->requestRefund($payment, $amount);
      }
      ErrorHelper::handleErrors($response);
    }
    catch (\Exception $e) {
      ErrorHelper::handleException($e);
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($amount->lessThan($payment->getBalance())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
    // Also save the order entity, because we have modified the 'data' field.
    $payment->getOrder()->save();

    $event = new PayoneRefundEvent($payment, $amount);
    $this->eventDispatcher->dispatch(CommercePayoneEvents::PAYONE_REFUND_FINISHED, $event);
  }

  /**
   * Returns an array populated with cart item data for API requests.
   *
   * Note that this always returns the full items. To list exact item list of
   * partial payments or refunds, custom event subscribers need to be
   * implemented in your project, as the Commerce Payment UI does not support
   * to declare explicit affected order items of partial payments or refunds.
   *
   * Currently there are only the 'commerce_payone.capture_request' and
   * 'commerce_payone.preauthorization_request' events, in the long run we
   * should introduce a dedicated event for the cart item related part, making
   * the clearing of certain items easier.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param \Drupal\commerce_price\Price|null $amount
   *   The amount to capture. If NULL, defaults to the entire payment amount.
   *
   * @return array
   *   An array populated with cart item data for API requests.
   */
  protected function getCartItemParameters(PaymentInterface $payment, Price $amount = NULL) {
    $order = $payment->getOrder();
    $parameters = [];
    /** @var \Drupal\commerce_price\Price[] $promotion_amounts_by_vat_rate */
    $promotion_amounts_by_vat_rate = [];

    if ($amount && $amount->lessThan($payment->getAmount())) {
      // This is a partial payment, so we cannot list the entire order items
      // here. The sums wouldn't match the payment amount and the Payone API
      // would return a "Article list faulty or incomplete" error. So we need to
      // send a fake item instead.
      $item_id = 1;
      $parameters[sprintf('it[%s]', $item_id)] = 'goods';
      $parameters[sprintf('id[%s]', $item_id)] = 'PARTIAL';
      $parameters[sprintf('pr[%s]', $item_id)] = $this->toMinorUnits($amount);
      $parameters[sprintf('no[%s]', $item_id)] = '1';
      $parameters[sprintf('de[%s]', $item_id)] = $this->t('Partial payment');
      $vat_rate = 0;
      $parameters[sprintf('va[%s]', $item_id)] = $vat_rate;
      $event = new PayoneCartItemParametersEvent($payment, $parameters, TRUE);
      $this->eventDispatcher->dispatch(CommercePayoneEvents::PAYONE_ALTER_CART_ITEM_PARAMETERS, $event);
      return $event->getParameters();
    }

    $item_id = 1;
    foreach ($order->getItems() as $item) {
      $purchased_entity = $item->getPurchasedEntity();
      $parameters[sprintf('it[%s]', $item_id)] = 'goods';
      $sku = $item->label();
      if ($purchased_entity instanceof ProductVariationInterface) {
        $sku = $purchased_entity->getSku();
      }
      elseif ($purchased_entity instanceof FieldableEntityInterface && $purchased_entity->hasField('sku') && !$purchased_entity->get('sku')->isEmpty()) {
        $sku = $purchased_entity->sku->value;
      }
      $parameters[sprintf('id[%s]', $item_id)] = $sku;
      $parameters[sprintf('pr[%s]', $item_id)] = $this->toMinorUnits($item->getAdjustedUnitPrice(['tax']));
      $parameters[sprintf('no[%s]', $item_id)] = Calculator::trim($item->getQuantity());
      $parameters[sprintf('de[%s]', $item_id)] = $item->label();
      $tax_adjustments = $item->getAdjustments(['tax']);
      $vat_rate = !empty($tax_adjustments) ? reset($tax_adjustments)->getPercentage() * 100 : 0;
      $parameters[sprintf('va[%s]', $item_id)] = $vat_rate;
      if ($promotion_adjustments = $item->getAdjustments(['promotion'])) {
        if (!isset($promotion_amounts_by_vat_rate[$vat_rate])) {
          $promotion_amounts_by_vat_rate[$vat_rate] = new Price('0', $item->getTotalPrice()->getCurrencyCode());
        }
        foreach ($promotion_adjustments as $promotion_adjustment) {
          $promotion_amounts_by_vat_rate[$vat_rate] = $promotion_amounts_by_vat_rate[$vat_rate]->add($promotion_adjustment->getAmount());
        }
      }
      $item_id++;
    }

    if ($order->hasField('shipments') && !$order->get('shipments')->isEmpty()) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface[] $shipments */
      $shipments = $order->get('shipments')->referencedEntities();
      foreach ($shipments as $shipment) {
        $parameters[sprintf('it[%s]', $item_id)] = 'shipment';
        $label = $this->t('Shipping');
        $parameters[sprintf('id[%s]', $item_id)] = 'shipping';
        // Always take the adjusted amount for shipping, as we don't get the
        // promotions via $shipment->getAdjustments() reliably.
        $parameters[sprintf('pr[%s]', $item_id)] = $this->toMinorUnits($shipment->getAdjustedAmount());
        $parameters[sprintf('no[%s]', $item_id)] = 1;
        $parameters[sprintf('de[%s]', $item_id)] = $label;
        $tax_adjustments = $shipment->getAdjustments(['tax']);
        $vat_rate = !empty($tax_adjustments) ? reset($tax_adjustments)->getPercentage() * 100 : 0;
        $parameters[sprintf('va[%s]', $item_id)] = $vat_rate;
        $item_id++;
      }
    }

    foreach ($promotion_amounts_by_vat_rate as $vat_rate => $promotion_amount) {
      $parameters[sprintf('it[%s]', $item_id)] = 'voucher';
      $label = $this->t('Discount');
      $parameters[sprintf('id[%s]', $item_id)] = 'discount';
      $parameters[sprintf('pr[%s]', $item_id)] = $this->toMinorUnits($promotion_amount);
      $parameters[sprintf('no[%s]', $item_id)] = 1;
      $parameters[sprintf('de[%s]', $item_id)] = $label;
      $parameters[sprintf('va[%s]', $item_id)] = $vat_rate;
      $item_id++;
    }

    $event = new PayoneCartItemParametersEvent($payment, $parameters);
    $this->eventDispatcher->dispatch(CommercePayoneEvents::PAYONE_ALTER_CART_ITEM_PARAMETERS, $event);
    return $event->getParameters();
  }

  /**
   * Returns the next sequence number for API requests.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   * @param int $minimal_sequence_no
   *   The minimal sequence number to set. Defaults to 1.
   * @param bool $update
   *   If the order data should be updated with the returned sequence number.
   *   Please note that the order entity won't get saved here. The caller must
   *   take care of saving the entity. This parameter just controls whether the
   *   field value gets set. Defaults to TRUE.
   *
   * @return int
   *   The next sequence number for API requests.
   */
  protected function getNextSequenceNumber(OrderInterface $order, int $minimal_sequence_no = 1, bool $update = TRUE) {
    $payment_gateway_data = $order->getData($this->getOrderDataKey());
    $sequence = $minimal_sequence_no;
    if (!empty($payment_gateway_data['last_sequence'])) {
      $sequence = (int) $payment_gateway_data['last_sequence'] + 1;
      if ($sequence < $minimal_sequence_no) {
        $sequence = $minimal_sequence_no;
      }
    }
    if ($update) {
      $payment_gateway_data['last_sequence'] = $sequence;
      $order->setData($this->getOrderDataKey(), $payment_gateway_data);
    }
    return $sequence;
  }

  /**
   * Performs the refund request for the given payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity to refund.
   * @param \Drupal\commerce_price\Price $amount
   *   The amount to refund.
   *
   * @return object
   *   The API response as JSON array.
   */
  protected function requestRefund(PaymentInterface $payment, Price $amount) {
    $order = $payment->getOrder();
    $request = $this->api->getServerApiStandardParameters($this->configuration, 'refund');
    $request['txid'] = $payment->getRemoteId();
    $minimal_sequence_number = $payment->getState()->getId() === 'partially_refunded' ? 3 : 2;
    $request['sequencenumber'] = $this->getNextSequenceNumber($order, $minimal_sequence_number);
    $request['amount'] = $this->toMinorUnits($amount->multiply('-1'));
    $request['currency'] = $amount->getCurrencyCode();

    // Allow modules to adjust the request.
    $event = new PayoneRequestEvent($payment, $request);
    $this->eventDispatcher->dispatch(CommercePayoneEvents::PAYONE_REFUND_REQUEST, $event);
    $request = $event->getRequest();

    return $this->api->processHttpPost($request, FALSE);
  }

  /**
   * Performs the debit request for the given payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity to refund.
   * @param \Drupal\commerce_price\Price $amount
   *   The amount to refund.
   *
   * @return object
   *   The API response as JSON array.
   */
  protected function requestDebit(PaymentInterface $payment, Price $amount) {
    $order = $payment->getOrder();
    $request = $this->api->getServerApiStandardParameters($this->configuration, 'debit');
    $request['txid'] = $payment->getRemoteId();
    $minimal_sequence_number = $payment->getState()->getId() === 'partially_refunded' ? 3 : 2;
    $request['sequencenumber'] = $this->getNextSequenceNumber($order, $minimal_sequence_number);
    $request['amount'] = $this->toMinorUnits($amount->multiply('-1'));
    $request['currency'] = $amount->getCurrencyCode();

    // Allow modules to adjust the request.
    $event = new PayoneRequestEvent($payment, $request);
    $this->eventDispatcher->dispatch(CommercePayoneEvents::PAYONE_DEBIT_REQUEST, $event);
    $request = $event->getRequest();

    return $this->api->processHttpPost($request, FALSE);
  }

  /**
   * Gets the key to be used to store Gateway specific info in the order data.
   *
   * As traits cannot implement interfaces, we need to add this as an abstract
   * function. Our plugins using this trait however all inherit from
   * PayonePaymentGatewayInterface.
   *
   * @return string
   *   The key to be used to store Gateway specific info in the order data.
   */
  abstract public function getOrderDataKey();

}
