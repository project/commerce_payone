<?php

namespace Drupal\commerce_payone\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Defines a common interface for Payone offsite payment gateways.
 */
interface PayoneOffsitePaymentGatewayInterface extends PayonePaymentGatewayInterface {

  /**
   * Creates and posts an (pre)authorization request for the given payment.
   *
   * The 'capture' setting of the payment process pane is respected. For
   * authorization only checkout flows, a preauthorization request is sent,
   * otherwise an authorization request.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param array $form
   *   The plugin form structure, must contain at least needed #return_url and
   *   #cancel_url keys.
   *
   * @return object
   *   The processed response from Payone API call.
   */
  public function initiatePaymentRequest(PaymentInterface $payment, array $form);

}
