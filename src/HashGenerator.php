<?php

namespace Drupal\commerce_payone;

/**
 * Defines the hash generator class.
 */
final class HashGenerator {

  /**
   * Constant for the md5 hash method.
   */
  const HASH_METHOD_MD5 = 'md5';

  /**
   * Constant for the sha2-384 hash method.
   */
  const HASH_METHOD_SHA2_384 = 'sha2-384';

  /**
   * Calculates the hash value required in Client API requests and return URLs.
   *
   * @param array $data
   *   The data used for hash generation.
   * @param string $security_key
   *   The security key as defined in the payment gateway configuration.
   * @param string $hash_method
   *   The hash method to use - one of 'md5', 'sha2-384'. Defaults to 'md5'.
   *
   * @return string
   *   The hash value required in Client API requests and return URLs.
   */
  public static function generateHash(array $data, string $security_key, $hash_method = self::HASH_METHOD_MD5) {
    // Sort by keys.
    ksort($data);

    // Hash code.
    $hash_str = '';
    foreach ($data as $key => $value) {
      $hash_str .= $data[$key];
    }

    switch ($hash_method) {
      case self::HASH_METHOD_SHA2_384:
        $hash = hash_hmac('sha384', $hash_str, $security_key);
        break;

      case self::HASH_METHOD_MD5:
      default:
        $hash_str .= $security_key;
        $hash = md5($hash_str);
    }

    return $hash;
  }

}
