<?php

namespace Drupal\commerce_payone\PluginForm\CreditCard;

use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm;
use Drupal\commerce_payone\HashGenerator;
use Drupal\commerce_payone\Plugin\Commerce\PaymentGateway\PayoneCreditCard;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides the payment method add form for Payone credit card payments.
 */
class CreditCardPaymentMethodAddForm extends PaymentMethodAddForm {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payone\Plugin\Commerce\PaymentGateway\PayoneCreditCardInterface $plugin */
    $plugin = $payment->getPaymentGateway()->getPlugin();
    $plugin_configuration = $plugin->getConfiguration();

    $data = [
      'request' => 'creditcardcheck',
      'responsetype' => 'JSON',
      'mode' => $plugin_configuration['mode'],
      'mid' => $plugin_configuration['merchant_id'],
      'aid' => $plugin_configuration['sub_account_id'],
      'portalid' => $plugin_configuration['portal_id'],
      'encoding' => 'UTF-8',
      'storecarddata' => 'yes',
    ];
    $hash_method = $plugin_configuration['hash_method'] ?? 'md5';
    $data['hash'] = HashGenerator::generateHash($data, $plugin_configuration['key'], $hash_method);

    $credit_card_types = CreditCard::getTypes();
    // Limit allowed cards, if configured.
    if (!empty($plugin_configuration['allowed_cards'])) {
      $allowed_types = array_flip($plugin_configuration['allowed_cards']);
      $credit_card_types = array_intersect_key($credit_card_types, $allowed_types);
    }
    $allowed_map = [];
    foreach (PayoneCreditCard::creditCardMap() as $payone_key => $card_key) {
      if (isset($allowed_types[$card_key])) {
        $allowed_map[$payone_key] = $card_key;
      }
    }
    $element['#attached']['library'][] = 'commerce_payone/form';
    $element['#attached']['drupalSettings']['commercePayone'] = [
      'request' => $data,
      'allowed_cards' => array_keys($allowed_map),
      'allowed_cards_map' => $allowed_map,
    ];
    $element['#attributes']['class'][] = 'payone-form';
    $element['#attributes']['class'][] = 'credit-card-form';
    $element['#id'] = 'payone-form';

    $element['card_types'] = [
      '#type' => 'container',
      '#attached' => [
        'library' => ['commerce_payment/payment_method_icons'],
      ],
    ];
    foreach ($credit_card_types as $credit_card_type) {
      $element['card_types'][$credit_card_type->getId()] = [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => [
          'id' => $credit_card_type->getId(),
          'class' => [
            'payment-method-icon',
            'payment-method-icon--' . $credit_card_type->getId(),
          ],
          'style' => '',
        ],
      ];
    }

    // Hidden elements.
    $element['pseudocardpan'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => ['pseudocardpan'],
      ],
    ];

    $element['truncatedcardpan'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => ['truncatedcardpan'],
      ],
    ];

    $element['cardexpiredate'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => ['cardexpiredateResponse'],
      ],
    ];

    $element['cardtype'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => ['cardtypeResponse'],
      ],
    ];

    $element['cardpaninputlabel'] = [
      '#type' => 'label',
      '#title' => $this->t('Card Number'),
      '#for' => 'cardpanInput',
    ];

    $element['cardpanplaceholder'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => [
        'class' => ['inputIframe'],
        'id' => 'cardpan',
      ],
    ];

    $element['cvcinputlabel'] = [
      '#type' => 'label',
      '#title' => $this->t('CVC'),
      '#for' => 'cvcInput',
    ];

    $element['cvcplaceholder'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => [
        'class' => ['inputIframe'],
        'id' => 'cardcvc2',
      ],
    ];

    $element['expireinputlabel'] = [
      '#type' => 'label',
      '#title' => $this->t('Expire date (mm/yyyy)'),
      '#for' => 'expireInput',
    ];

    $element['expiration'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'expireInput',
        'class' => ['inputIframe', 'credit-card-form__expiration'],
      ],
    ];

    $element['expiration']['month'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => [
        'id' => 'cardexpiremonth',
      ],
    ];

    $element['expiration']['divider'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => [
        'class' => ['credit-card-form__divider'],
      ],
    ];

    $element['expiration']['year'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => [
        'id' => 'cardexpireyear',
      ],
    ];

    $element['error'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => [
        'id' => 'error',
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    $values = $form_state->getValue($element['#parents']);

    if (empty($values['pseudocardpan'])) {
      $form_state->setError($element, $this->t('Card data could not be retrieved.'));
    }
    if (empty($values['cardtype'])) {
      $form_state->setError($element, $this->t('Card type could not be retrieved.'));
    }
    if (empty($values['truncatedcardpan'])) {
      $form_state->setError($element, $this->t('Card data could not be retrieved.'));
    }
    if (empty($values['cardexpiredate'])) {
      $form_state->setError($element, $this->t('Card expire date could not be retrieved.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    // The payment gateway plugin will process the submitted payment details.
  }

}
