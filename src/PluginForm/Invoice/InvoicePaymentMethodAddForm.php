<?php

namespace Drupal\commerce_payone\PluginForm\Invoice;

use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the payment method add form for invoice payments.
 */
class InvoicePaymentMethodAddForm extends PaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['payment_details'] = $this->buildInvoiceMethodForm($form['payment_details'], $form_state);

    return $form;
  }

  /**
   * Builds the invoice method form.
   *
   * @param array $element
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built invoice method form.
   */
  protected function buildInvoiceMethodForm(array $element, FormStateInterface $form_state) {
    $element['dob'] = [
      '#type' => 'date',
      '#title' => $this->t('Date of birth'),
      '#required' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->submitInvoiceMethodForm($form['payment_details'], $form_state);

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Handles the submission of the invoice method form.
   *
   * @param array $element
   *   The invoice method form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function submitInvoiceMethodForm(array $element, FormStateInterface $form_state) {
    $values = $form_state->getValue($element['#parents']);
    $this->entity->dob = $values['dob'];
  }

}
