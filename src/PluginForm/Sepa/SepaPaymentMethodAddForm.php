<?php

namespace Drupal\commerce_payone\PluginForm\Sepa;

use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the payment method add form for SEPA payments.
 */
class SepaPaymentMethodAddForm extends PaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['payment_details'] = $this->buildSepaMethodForm($form['payment_details'], $form_state);

    return $form;
  }

  /**
   * Builds the SEPA method form.
   *
   * @param array $element
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built SEPA method form.
   */
  protected function buildSepaMethodForm(array $element, FormStateInterface $form_state) {
    $element['iban'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IBAN'),
      '#required' => TRUE,
    ];

    $element['bic'] = [
      '#type' => 'textfield',
      '#title' => $this->t('BIC'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->submitSepaForm($form['payment_details'], $form_state);

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Handles the submission of the SEPA form.
   *
   * @param array $element
   *   The SEPA form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function submitSepaForm(array $element, FormStateInterface $form_state) {
    $values = $form_state->getValue($element['#parents']);
    $this->entity->iban = $values['iban'];
    $this->entity->bic = $values['bic'];
  }

}
