<?php

namespace Drupal\commerce_payone\PluginForm\Paypal;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the offsite payment form for PayPal payments.
 */
class PaypalOffsiteForm extends PaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_payone\Plugin\Commerce\PaymentGateway\PayonePaypalInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;

    $response = $payment_gateway_plugin->initiatePaymentRequest($payment, $form);

    if ($response->status != 'REDIRECT') {
      throw new PaymentGatewayException(sprintf('Paypal error (code %s): %s', $response->errorcode, $response->errormessage));
    }

    $transaction_id = $response->txid;
    $payment->setRemoteId($transaction_id);
    $payment->state = 'preauthorization';
    $payment->save();

    $order = $payment->getOrder();
    $options = UrlHelper::parse($response->redirecturl);
    $order->setData($payment_gateway_plugin->getOrderDataKey(), [
      'transaction_id' => $transaction_id,
      'token' => $options['query']['token'],
      'capture' => $form['#capture'],
    ]);
    $order->save();

    return $this->buildRedirectForm($form, $form_state, $response->redirecturl, []);
  }

}
