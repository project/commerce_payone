<?php

namespace Drupal\commerce_payone;

/**
 * Defines the interface for the Payone API call service layer.
 */
interface PayoneApiServiceInterface {

  /**
   * Fills and returns an array with mandatory parameters for any Client API.
   *
   * @param array $plugin_configuration
   *   The payment gateway plugin configuration.
   * @param string $request_method
   *   The request method, eg. "authorization", "capture".
   * @param string $response_type
   *   The response type. Defaults to 'JSON'.
   *
   * @return array
   *   An array with mandatory parameters for any Client API.
   *
   * @see https://docs.payone.com/display/public/PLATFORM/CA+-+General+Interface+Definitions#CA-GeneralInterfaceDefinitions-Standardparameter
   */
  public function getClientApiStandardParameters(array $plugin_configuration, string $request_method, string $response_type = 'JSON');

  /**
   * Fills and returns an array with mandatory parameters for any Server API.
   *
   * @param array $plugin_configuration
   *   The payment gateway plugin configuration.
   * @param string $request_method
   *   The request method, eg. "authorization", "capture".
   *
   * @return array
   *   An array with mandatory parameters for any Server API.
   *
   * @see https://docs.payone.com/display/public/PLATFORM/SA+-+General+Interface+Definitions#SA-GeneralInterfaceDefinitions-Standardparameter
   */
  public function getServerApiStandardParameters(array $plugin_configuration, string $request_method);

  /**
   * Calculates the hash value required in Client API requests and return URLs.
   *
   * @param array $data
   *   The data used for hash generation.
   * @param string $security_key
   *   The security key as defined in the payment gateway configuration.
   * @param string $hash_method
   *   The hash method to use - one of 'md5', 'sha2-384'. Defaults to 'md5'.
   *
   * @return string
   *   The hash value required in Client API requests and return URLs.
   */
  public function generateHash(array $data, string $security_key, $hash_method = HashGenerator::HASH_METHOD_MD5);

  /**
   * Sends and processes Payone API request.
   *
   * @param array $request_parameters
   *   The request parameters to send to Payone API.
   * @param bool $client_api
   *   Whether or not the Client API should be called (otherwise the Server API
   *   is called). Defaults to TRUE.
   *
   * @return object
   *   The processed and decoded JSON response of the API call as object.
   */
  public function processHttpPost(array $request_parameters, bool $client_api = TRUE);

}
