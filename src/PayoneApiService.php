<?php

namespace Drupal\commerce_payone;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\ClientInterface;

/**
 * Default Payone API service implementation.
 */
class PayoneApiService implements PayoneApiServiceInterface {

  /**
   * The client API endpoint URL.
   */
  const CLIENT_API_URL = 'https://secure.pay1.de/client-api/';

  /**
   * The server API endpoint URL.
   */
  const SERVER_API_URL = 'https://api.pay1.de/post-gateway/';

  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a new PayoneApiService object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public function getClientApiStandardParameters(array $plugin_configuration, string $request_method, string $response_type = 'JSON') {
    return [
      'mid' => $plugin_configuration['merchant_id'],
      'portalid' => $plugin_configuration['portal_id'],
      'api_version' => '3.10',
      'mode' => $plugin_configuration['mode'],
      'request' => $request_method,
      'responsetype' => $response_type,
      'encoding' => 'UTF-8',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getServerApiStandardParameters(array $plugin_configuration, string $request_method) {
    return [
      'mid' => $plugin_configuration['merchant_id'],
      'portalid' => $plugin_configuration['portal_id'],
      'key' => md5($plugin_configuration['key']),
      'api_version' => '3.10',
      'mode' => $plugin_configuration['mode'],
      'request' => $request_method,
      'encoding' => 'UTF-8',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function generateHash(array $data, string $security_key, $hash_method = HashGenerator::HASH_METHOD_MD5) {
    return HashGenerator::generateHash($data, $security_key, $hash_method);
  }

  /**
   * {@inheritdoc}
   */
  public function processHttpPost(array $request_parameters, bool $client_api = TRUE) {
    $url = $client_api ? self::CLIENT_API_URL : self::SERVER_API_URL;

    try {
      $response = $this->httpClient->post($url, [
        'form_params' => $request_parameters,
      ]);
    }
    catch (ClientException $e) {
      $response = $e->getResponse();
      $data = json_decode($response->getBody()->getContents());
      // TODO: handle multiple errors.
      throw new \Exception($data->errors[0]->message, $e->getCode(), $e);
    }
    catch (ConnectException $e) {
      throw new \Exception($e->getMessage(), $e->getCode(), $e);
    }

    if ($client_api) {
      $response_result = $response->getBody()->getContents();
    }
    else {
      $response_result = explode("\n", trim($response->getBody()->getContents()));
      $response_result = $this->parseResponse($response_result);
    }

    return json_decode($response_result);
  }

  /**
   * Parses the given raw response data and returns a valid JSON string.
   *
   * Lines containing key-value pairs separated by '=' get converted to arrays
   * and will be represented as such, when resulting JSON string gets decoded
   * later.
   *
   * @todo Investigate, why this function has been implemented and if we still
   * need to do this!
   *
   * @param array $raw_response
   *   The raw response, having the single lines exploded into an array.
   *
   * @return string
   *   A JSON encoded string representation of the given response data.
   */
  protected function parseResponse(array $raw_response = []) {
    $result = [];

    if (count($raw_response) === 0) {
      return json_encode($result);
    }

    foreach ($raw_response as $key => $line) {
      $pos = strpos($line, "=");

      if ($pos === FALSE) {
        if (strlen($line) > 0) {
          $result[$key] = $line;
        }
        continue;
      }

      $line_array = explode('=', $line);
      $result_key = array_shift($line_array);
      $result[$result_key] = implode('=', $line_array);
    }

    return json_encode($result);
  }

}
